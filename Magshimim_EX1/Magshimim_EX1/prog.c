﻿#include <stdio.h>
#include <stdlib.h>
#include <opencv2\highgui\highgui_c.h>
#include "view.h"
//חיפוש
//הכנסת תו ריק
//יותר גדול מאורך הרשימה
int main(void)
{

	char name[20] = { 0 };
	int index;
	int duration = 0;
	int choice = 0, load = 0;
	FrameNode * head = 0;
	FrameNode * frameTemp = 0;
	printf("[0] - opent an excit project\n");
	printf("[1] - create new project\n");
	printf("Enter a choice: ");
	scanf_s("%d", &load);
	getchar();//cleaning the buffer
	if (load == 0)
	{
		openProject(&head);
	}
	do
	{
		printf("[0] - exit\n");
		printf("[1] - add new frame\n");
		printf("[2] - remove a frame\n");
		printf("[3] - change frame index\n");
		printf("[4] - change frame duration\n");
		printf("[5] - change duration of all\n");
		printf("[6] - list frames\n");
		printf("[7] - play movie!\n");
		printf("[8] - save project.\n");
		printf("[9] - Play the video in reverse.\n");
		printf("[10] - Play the video grayscale.\n");
		printf("Enter a choice: ");
		scanf_s("%d", &choice);
		getchar();//cleaning the buffer
		switch (choice)
		{
		case 1:
			add(&head, getDetails(head));
			break;
		case 2:
			printf("Enter the name of the frame do you to erase\n");
			fgets(name, 50, stdin);
			if (searchName(name, head))
			{
				deleteNode(&head, name);
			}
			else
			{
				printf("Frame is not exict.\n");
			}
			break;
		case 3:
			printf("Enter the name of the frame do you to erase\n");
			fgets(name, 50, stdin);
			if (searchName(name, head))
			{
				printf("Enter the new index in the movie you wish to place the frame: ");
				scanf_s("%d", &index);
				getchar();
				changePos(&head, name, index);
			}
			else
			{
				printf("Frame is not exict.\n");
			}
			break;
		case 4:
			printf("Enter the name of the frame: ");
			fgets(name, 50, stdin);
			if (searchName(name, head))
			{
				changeDuration(head, name);
			}
			else
			{
				printf("Frame is not exict.\n");
			}
			break;
		case 5:
			printf("Enter the new duration: ");
			scanf_s("%d", &duration);
			changeAllDuration(head, duration);
			break;
		case 6:
			printList(head);
			break;
		case 7:
			play(head,7);
			break;
		case 8:
			saveProjectInFile(head);
			break;
		case 9:
			reverse(&head);
			play(head,9);
			reverse(&head);
			break;
		case 10:
			play(head, 10);
			break;
		}
	} while (choice != 0);
	free(head);
	//C:\images\sane.txt
	//‪
	getchar();
	return 0;
}