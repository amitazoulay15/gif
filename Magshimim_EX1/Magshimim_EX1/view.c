#include "view.h"
#include <string.h>
#define NUM_FRAME_1 "**** FRAME "
#define NUM_FRAME_2 "****"
/*
play the movie!!
display the images each for the duration of the frame one by one and close the window
input: a linked list of frames to display
output: none
*/
void play(FrameNode* list,int choice)
{
	cvNamedWindow("Display window", CV_WINDOW_AUTOSIZE); //create a window
	FrameNode* head = list;
	int imgNum = 1, playCount = 0;
	IplImage* image;
	char * pos;
	int i = 0;
	while (playCount < GIF_REPEAT)
	{
		while (list != 0)
		{
			
			if ((pos = strchr(&(list->frame->path), '\n')) != NULL)
			{
				*pos = '\0';
			}
			if (choice != 10)
			{
				image = cvLoadImage(&(list->frame->path), 1);
			}
			else
			{
				image = cvLoadImage(&(list->frame->path), 0);
			}
			IplImage* pGrayImg = 0;
			for (i = 0; i < 5; i++)
			{
				pGrayImg = cvCreateImage(cvSize(image->width, image->height), image->depth, 1);
			}
			
			if (!image) //The image is empty - shouldn't happen since we checked already.
			{
				printf("Could not open or find image number %d", imgNum);
			}
			else
			{
				cvShowImage("Display window", image); //display the image
				cvWaitKey(list->frame->duration); //wait
				list = list->next;
				cvReleaseImage(&image);
			}
			imgNum++;
		}
		list = head; // rewind
		playCount++;
	}
	cvDestroyWindow("Display window");
	return;
}
/*
This funcnion add a frame to the linked list at the end. 
input head,newNode:the linked list of the frames, new frame 
type head,newNode: FrameNode,FrameNode
return: None
rtype:none
*/
void add(FrameNode ** head, FrameNode *newNode)
{
	FrameNode* frame = *head;
	if (!*head)
	{
		*head = newNode;
	}
	else
	{
		while (frame->next)
		{
			frame = frame->next;
		}
		frame->next = newNode;
		newNode->next = NULL;
	}

}
/*
This funcnion get details of frame and build a new node(frame)
input:none
type none: none
return:newFrame
rtype:FrameNode
*/
FrameNode * getDetails(FrameNode * head)
{
	Frame * frame = (Frame*)malloc(sizeof(Frame));

	printf("Enter the frame name: ");
	fgets(&(frame->name), 50, stdin);
	if (head)
	{
		while (searchName(&(frame->name), head) != 0)
		{
			printf("The name is already taken please enter another name: ");
			fgets(&(frame->name), 50, stdin);
		}
	}

	printf("Enter the frame duration: ");
	scanf("%d", &frame->duration);
	getchar();//cleaning the buffer
	printf("Enter the frame path: ");
	fgets(&(frame->path), 20, stdin);
	
	FrameNode * newFrame = (FrameNode*)malloc(sizeof(FrameNode));
	newFrame->frame = frame;
	newFrame->next = NULL;

	return newFrame;
	
}
/*
This function open node fora node in a project that excit
intput name,duration,path: name frame, duration frame , path frame
type name,duration,path: string, int, string
return newFrame : "old new" frame
rtype: FrameNode
*/
FrameNode * createNode(char name[], int duration, char path[])
{
	Frame * frame = (Frame*)malloc(sizeof(Frame));
	strcpy(frame->name, name);
	frame->duration = duration;
	strcpy(frame->path, path);

	FrameNode * newFrame = (FrameNode*)malloc(sizeof(FrameNode));
	newFrame->frame = frame;
	newFrame->next = NULL;

	return newFrame;
}
/*
This function open a project that exict
input head: the linked list of the frames
type head:FrameNode
return:none
rtype:none
*/
void openProject(FrameNode ** head)
{
	int char_from_file = 0, index = 0,count = 0, i = 0, duration = 0;
	char my_str[50] = { 0 };
	char filePath[50] = { 0 };
	char name[50] = { 0 };
	char path[50] = { 0 };
	char * pos;
	printf("Enter the path of the file from which you want to open the project\n");
	fgets(filePath, 50, stdin);
	
	if ((pos = strchr(filePath, '\n')) != NULL)//remove the '\n'
	{
		*pos = '\0';
	}
		
	FILE * file = fopen(filePath, "r+");
	file_exist(file);
	
	
	do
	{
		char_from_file = fgetc(file);

		if (char_from_file == 45)
		{
			while (char_from_file != '\n')
			{
				char_from_file = fgetc(file);
				my_str[i] = char_from_file;
				i++;
			}
			my_str[i] = 0;
			count++;
			if (count == 1)
			{
				strcpy(name, my_str);
			}
			else if (count == 2)
			{
				duration = atoi(my_str);
			}
			else
			{
				strcpy(path, my_str);
				FrameNode * curr = createNode(name, duration, path);
				add(&(*head), curr);
				count = 0;
			}
			i = 0;
		}
		

	} while (char_from_file != EOF);
	fclose(file);

}
/*
This funcnion print the frames in the linked list
input list: the linked list of the frames
type list:FrameNode
return: None
rtype:none
*/
void printList(FrameNode* list)
{
	char *pos;
	if (list)
	{
		if ((pos = strchr(&(list->frame->name), '\n')) != NULL)//remove the '\n'
		{
			*pos = '\0';
		}	
		printf("Frame name: %s\n", &(list->frame->name));
		printf("Frame duration: %d\n", list->frame->duration);
		printf("Frame path: %s\n", &(list->frame->path));
		printList(list->next);//Recursively
	}
	else
	{
		printf("\n");
	}
}
/*
this function delete frame from the linked list
input head,name: the linked list of the frames,the name of the frame to delete
type head,name: FrameNode,string
return:none
rtype:none
*/
void deleteNode(FrameNode** head, char* name)
{
	FrameNode* frame = *head;
	FrameNode* temp = NULL;

	
	if (*head)// if the list is not empty (if list is empty - nothing to delete!)
	{
		// the first node should be deleted?
		if (0 == strcmp(&((*head)->frame->name), name))
		{
			*head = (*head)->next;
			free(frame);
		}
		else
		{
			while (frame->next)
			{
				if ((0 == strcmp(&(frame->next->frame->name), name))) // waiting to be on the node BEFORE the one we want to delete
				{
					temp = frame->next; // put aside the node to delete
					frame->next = temp->next; // link the node before it, to the node after it
					free(temp); // delete the node
				}
				else
				{
					frame = frame->next;
				}
			}
		}
	}
}
/*
this function change the duration of a frame.
input head,name: the linked list of the frames,the name of the frame to chang duration
type head,name: FrameNode,string
return:none
rtype:none
*/
void changeDuration(FrameNode * head,char name[20])
{
	if(head)
	{
		if (!(strcmp(&(head->frame->name), name)))
		{
			int duration = 0;
			printf("Enter the new duration: ");
			scanf("%d", &duration);
			head->frame->duration = duration;
		}
		else
		{
			changeDuration(head->next, name);
		}
	}
}
/*
this function change the duration of all the frames by a new duration that the user entered.
input head,duration: the linked list of the frames,new duration
type head,name: FrameNode,int
return:none
rtype:none
*/
void changeAllDuration(FrameNode * head,int duration)
{
	if (head) 
	{
		head->frame->duration = duration;
		changeAllDuration(head->next,duration);
	}
}
/*
this function search a name of frame in the linked list
input head,name: the linked list of the frames,the name of the frame to chang duration
type head,name: FrameNode,string
return: true or false
rtype: bool
*/
FrameNode * searchName(char name[], FrameNode *head)
{

	FrameNode *current_node = head;
	char * pos;
	for (current_node = head; current_node != NULL; current_node = current_node->next)
	{
		if ((pos = strchr(name, '\n')) != NULL)//remove the '\n'
		{
			*pos = '\0';
		}
		if ((pos = strchr(&(current_node->frame->name), '\n')) != NULL)//remove the '\n'
		{
			*pos = '\0';
		}
		if (strcmp(name, &(current_node->frame->name)) == 0)
		{
			return 1;//return True
		}
	}
	return 0;//return False
}
/*
this function change a posstion of frame in the linked list
input head,name,pos: the linked list of the frames,the name of the frame to change index, the new index of the frame
type head,name,pos; FrameNode,const char,index
return:none
rtype:none
*/
void changePos(FrameNode **head, const char *name, int index)
{
	assert(head != 0 && name != 0 && index >= 0);
	FrameNode *root = *head;
	FrameNode *link = root;
	FrameNode *prev = 0;
	int count = 0;
	while (link != 0 && strcmp(&(link->frame->name), name) != 0)
	{
		prev = link;
		link = link->next;
		count++;
	}
	if (link == 0)
	{
		return;
	}
	if (count == index)
	{
		return;
	}
		
	if (count == 0)    
	{
		assert(link == root);
		*head = root->next;
		root = *head;
	}
	else
	{
		assert(prev != 0);
		prev->next = link->next;
	}
	
	if (index == 0)  
	{
		link->next = root;
		*head = link;
		return;
	}
	FrameNode *node = root;
	for (int i = 0; i < index - 1 && node->next != 0; i++)
	{
		node = node->next;
	}
	link->next = node->next;
	node->next = link;
}
/*
this fuction save the project(all the frame) in a file that the user decied whose name.
input head: the linked list of the frames
type head: FrameNode
return:none
rtype:none
*/
void  saveProjectInFile(FrameNode *head)
{
	char * pos;
	char buffer[10] = { 0 };
	char folder[20] = { 0 };
	char fileToSave[20] = { 0 };
	char name[20] = "name -";
	char duration[20] = "duration -";
	char path[20] = "path -";
	
	int i = 1;
	FrameNode * curr = head;
	name[7] = '\0';
	duration[11] = '\0';
	path[7] = '\0';
	printf("Enter folder to save thw project: ");
	fgets(folder, 20, stdin);
	printf("Enter the name of the file to seva in is the project: ");
	fgets(fileToSave, 20, stdin);

	if ((pos = strchr(folder, '\n')) != NULL)//remove the '\n'
	{
		*pos = '\0';
	}
	/*chaning "folder" to a new file path*/
	strcat(folder, "\\");
	strcat(folder, "\\");
	strcat(folder, fileToSave);
	
	if ((pos = strchr(folder, '\n')) != NULL)//remove the '\n'
	{
		*pos = '\0';
	}
		
	FILE * file = fopen(folder,"w+");
	file_exist(file);

	while (curr)
	{
		write_to_file(NUM_FRAME_1, file);
		sprintf(buffer, "%d", i);
		write_to_file(buffer, file);
		write_to_file(NUM_FRAME_2, file);
		write_to_file("\n", file);
		write_to_file(name, file);
		if ((pos = strchr(curr->frame->name, '\n')) != NULL)//remove the '\n'
		{
			*pos = '\0';
		}
		write_to_file(curr->frame->name, file);
		write_to_file("\n", file);
		write_to_file(duration, file);
		sprintf(buffer, "%d", curr->frame->duration);
		write_to_file(buffer, file);
		write_to_file("\n", file);
		write_to_file(path, file);
		write_to_file(curr->frame->path, file);
		write_to_file("\n", file);
		curr = curr->next;
		i++;
	}
	fclose(file);
}
/*
this fuction write to a file
input str,file: the string to write to the file, the file path
type file,str: FILE,string
return: void
*/
void write_to_file(char str[], FILE * file)
{
	int i = 0;
	for (i = 0; i < strlen(str); i++)
	{
		fputc(str[i], file);//write to the file
	}
}
/*
This function check if the file that is opnened are exict.
input file: file path
type file: FILE
return:none
rtype:none
*/
void file_exist(file)
{
	if (file == NULL)
	{
		printf("Your file does not exist.\n");
		getchar();
		exit(1);
	}
}
/*
This function reverse a stirng recursively.
input head:linked list of frames.
type head: FrameNode
return:none
rtype:none
*/
void reverse(FrameNode** head)
{
	FrameNode* first;
	FrameNode* rest;
	if (*head == NULL)
	{
		return;
	}	
	first = *head;
	rest = first->next;
	if (rest == NULL)
	{
		return;
	}	
	reverse(&rest);
	first->next->next = first;

	first->next = NULL;

	*head = rest;
}
