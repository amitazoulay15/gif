/*********************************
* Class: MAGSHIMIM Final Project *
* Play function declaration	 	 *
**********************************/
#ifndef VIEWH
#define VIEWH

#include <opencv2\core\core_c.h>
#include <opencv2\highgui\highgui_c.h>
#include <stdio.h>
#include "LinkedList.h"

#define GIF_REPEAT 5

void play(FrameNode* list, int choice);
void add(FrameNode ** head, FrameNode *newNode);
FrameNode * getDetails(FrameNode * head);
void printList(FrameNode* list);
void deleteNode(FrameNode** head, char* name);
void changeDuration(FrameNode * head, char name[20]);
void changeAllDuration(FrameNode * head, int duration);
void changePos(FrameNode **head, const char *name, int index);
FrameNode * searchName(char name[], FrameNode *head);
void  saveProjectInFile(FrameNode *head);
void file_exist(file);
void write_to_file(char str[], FILE * file);
void openProject(FrameNode ** head);
FrameNode * createNode(char name[], int duration, char path[]);
void reverse(FrameNode** head);
#endif